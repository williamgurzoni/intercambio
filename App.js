import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import firebaseConfig from './src/js/firebase-config';
import AppRoutes from './src/js/routes';
import reducers from './src/reducers';
import NavigationService from './src/js/navigation-service';

console.disableYellowBox = true;

class AppWrapper extends Component {
  constructor(props) {
    super(props);
    firebase.initializeApp(firebaseConfig);
  }

  render() {
    return (
      <Provider store={ createStore(reducers, {}, applyMiddleware(ReduxThunk)) }>
        <AppRoutes
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
      </Provider>
    );
  }
}

export default AppWrapper;
