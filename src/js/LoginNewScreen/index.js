import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { connect } from 'react-redux';

import {
  changeName,
  changeLogin,
  changePassword,
  createNewUser,
} from '../../actions/LoginActions';

import styles from '../LoginScreen/style';

const homeBg = require('../WelcomeScreen/assets/welcome-bg.jpg');

class LoginNewScreen extends Component<props> {
  newUser() {
    const { name, login, password } = this.props;
    this.props.createNewUser(name, login, password);
  }

  render() {
    return (
        <View style={ { flex: 1 } }>
          <ImageBackground source={ homeBg } style={ styles.ImageBackground }>
            <ScrollView>
              <View style={{ alignSelf: 'center', flex: 1, alignItems: 'stretch', width: 280 }}>
                <Text style={{ fontSize: 28, fontWeight: 'bold', color: '#FFFFFF', alignSelf: 'center' }}>
                  Intercâmbio Planner
                </Text>
                <Text style={{ textAlign: 'center', fontSize: 10, marginBottom: 50 }}>
                  @passagempradois
                </Text>
                <TextInput
                  value={ this.props.name }
                  placeholder='Nome'
                  returnKeyType='next'
                  onChangeText={ name => this.props.changeName(name) }
                  style={ styles.TextInput }
                />
                <TextInput
                  value={ this.props.login }
                  placeholder='Usuário'
                  returnKeyType='next'
                  onChangeText={ login => this.props.changeLogin(login) }
                  style={ styles.TextInput }
                />
                <TextInput
                  value={ this.props.password }
                  placeholder='Senha'
                  returnKeyType='done'
                  onChangeText={ password => this.props.changePassword(password) }
                  style={ styles.TextInput }
                  secureTextEntry
                />
                <TouchableOpacity
                  style={ styles.btnStart }
                  onPress={ () => { this.newUser(); } }>
                  <View style={ styles.bgBtnStart }>
                    <Text style={ styles.txtBtnStart }>Agora sim, PARTIU!</Text>
                  </View>
                </TouchableOpacity>
                <Text
                  style={ this.props.newUserErrorMessage ? styles.errorMessage : null }
                >
                  { this.props.newUserErrorMessage }
                </Text>
              </View>
            </ScrollView>
          </ImageBackground>
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    name: state.LoginReducer.name,
    login: state.LoginReducer.login,
    password: state.LoginReducer.password,
    newUserErrorMessage: state.LoginReducer.newUserErrorMessage,
  });
};

const mapDispatchToProps = {
  changeName,
  changeLogin,
  changePassword,
  createNewUser,
};

export default connect(mapStateToProps, { ...mapDispatchToProps })(LoginNewScreen);
