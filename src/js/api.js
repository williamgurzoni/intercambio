import axios from 'react-native-axios';

export const resolve = async (promise) => {
  const resolved = {
    data: null,
    error: null,
  };
  try {
    resolved.data = await promise;
  } catch (e) {
    resolved.error = e;
  }
  return resolved;
};

export const currencyApi = (fromCurrency, toCurrency) => {
  let baseUrl = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE';
  const apikey = '5GZO88HMX8GTTKMQ';
  baseUrl += `&from_currency=${fromCurrency}`;
  baseUrl += `&to_currency=${toCurrency}`;
  baseUrl += `&apikey=${apikey}`;

  return resolve(axios.get(baseUrl));
};

export const weatherApi = (cityId) => {
  let baseUrl = 'https://api.openweathermap.org/data/2.5/weather?q=';
  const apikey = '672444815777c077b47cb7c983eeb6b8';
  baseUrl += `&id=${cityId}`;
  baseUrl += `&appid=${apikey}`;
  baseUrl += '&units=metric';

  return resolve(axios.get(baseUrl));
};
