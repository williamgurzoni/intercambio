import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  Text,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import firebase from 'firebase';

import styles from './style';

const homeBg = require('./assets/welcome-bg.jpg');
const rectangle = require('./assets/rectangle.png');

class WelcomeScreen extends Component<props> {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        //this.props.navigation.navigate('Home');
      }
      this.setState({ isLoading: false });
    });
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <ActivityIndicator size='large' />
        </View>
      );
    }
    return (
        <View style={ { flex: 1 } }>
          <ImageBackground source={ homeBg } style={ styles.content }>
            <ScrollView>
              <ImageBackground source={ rectangle } style={ styles.bgWelcome }>
                <Text style={ styles.txtWelcome }>Intercâmbio</Text>
                <Text style={ styles.txtWelcomeSub }>Planejar não é tudo, mas é quase...</Text>
              </ImageBackground>
              <TouchableOpacity
                style={ styles.btnStart }
                onPress={ () => this.props.navigation.navigate('Login') }
              >
                <ImageBackground style={ styles.bgBtnStart } source={ require('./assets/btnStart.png') }>
                  <Text style={ styles.txtBtnStart }>Iniciar Planejamento</Text>
                </ImageBackground>
              </TouchableOpacity>
            </ScrollView>
          </ImageBackground>
        </View>
    );
  }
}

export default WelcomeScreen;
