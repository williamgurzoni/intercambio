import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    alignItems: 'center',
  },
  bgWelcome: {
    flex: 2,
    height: 221,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 150,
  },
  txtWelcome: {
    color: '#FFFFFF',
    fontSize: 48,
    fontFamily: 'Roboto',
  },
  txtWelcomeSub: {
    color: '#FFFFFF',
    fontSize: 18,
    fontFamily: 'Roboto',
    marginTop: 13,
  },
  btnStart: {
    marginTop: 100,
    justifyContent: 'center',
  },
  bgBtnStart: {
    width: 345,
    height: 67,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtBtnStart: {
    color: '#353434',
    fontSize: 19,
  },
});

export default styles;
