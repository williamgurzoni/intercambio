import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  ImageBackground: {
    flex: 1,
    alignItems: 'stretch',
    paddingTop: 90,
  },
  TextInput: {
    backgroundColor: '#F6F6F6',
    fontSize: 17,
    color: '#828282',
    justifyContent: 'center',
    height: 50,
    marginVertical: 15,
    textAlign: 'center',
  },
  btnStart: {
    marginTop: 20,
    justifyContent: 'center',
    paddingHorizontal: 20,
    height: 54,
  },
  bgBtnStart: {
    backgroundColor: '#F2C94C',
    height: 54,
    borderRadius: 30,
    justifyContent: 'center',
  },
  bgBtnStartNew: {
    backgroundColor: '#2F80ED',
    height: 54,
    borderRadius: 30,
    justifyContent: 'center',
  },
  txtBtnStart: {
    color: '#353434',
    fontSize: 19,
    alignSelf: 'center',
  },
  txtBtnStartNew: {
    color: '#FFFFFF',
    fontSize: 19,
    alignSelf: 'center',
  },
  errorMessage: {
    color: '#AA0000',
    fontSize: 19,
    padding: 10,
    marginTop: 15,
    alignSelf: 'center',
    backgroundColor: '#FFFFFF55',
  },
  wizardTextInput: {
    backgroundColor: '#F6F6F6',
    color: '#828282',
    height: 50,
    marginBottom: 15,
    marginTop: 5,
  },
});

export default styles;
