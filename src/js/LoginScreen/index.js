import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  Text,
  ScrollView,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { connect } from 'react-redux';

import { changeLogin, changePassword, signIn } from '../../actions/LoginActions';
import styles from './style';

const homeBg = require('./assets/welcome-bg.jpg');

class LoginScreen extends Component<props> {
  login() {
    const { login, password } = this.props;
    this.props.signIn(login, password);
  }

  render() {
    return (
        <View style={ { flex: 1 } }>
          <ImageBackground source={ homeBg } style={ styles.ImageBackground }>
            <ScrollView style={{ flex: 1 }}>
              <View style={{ alignSelf: 'center', flex: 1, alignItems: 'stretch', width: 280 }}>
                <Text style={{ fontSize: 28, fontWeight: 'bold', color: '#FFFFFF', alignSelf: 'center' }}>
                  Intercâmbio Planner
                </Text>
                <Text style={{ textAlign: 'center', fontSize: 10, marginBottom: 50 }}>
                  @passagempradois
                </Text>
                <TextInput
                  value={ this.props.login }
                  placeholder='Usuário'
                  returnKeyType='done'
                  onChangeText={ login => this.props.changeLogin(login) }
                  style={ styles.TextInput }
                />
                <TextInput
                  value={ this.props.password }
                  placeholder='Senha'
                  returnKeyType='done'
                  onChangeText={ password => this.props.changePassword(password) }
                  style={ styles.TextInput }
                  secureTextEntry
                />
                <TouchableOpacity
                  style={ styles.btnStart }
                  onPress={ () => { this.login(); } }>
                  <View style={ styles.bgBtnStart }>
                    <Text style={ styles.txtBtnStart }>Partiu</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  style={ styles.btnStart }
                  onPress={ () => this.props.navigation.navigate('LoginNew') }>
                  <View style={ styles.bgBtnStartNew }>
                    <Text style={ styles.txtBtnStartNew }>Sou novo aqui</Text>
                  </View>
                </TouchableOpacity>
                <Text
                  style={ this.props.loginErrorMessage ? styles.errorMessage : null }
                >
                  { this.props.loginErrorMessage }
                </Text>
                <Text
                  style={ this.props.uid ? styles.errorMessage : null }
                >
                  { this.props.uid }
                </Text>
              </View>
            </ScrollView>
          </ImageBackground>
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    login: state.LoginReducer.login,
    password: state.LoginReducer.password,
    loginErrorMessage: state.LoginReducer.loginErrorMessage,
    uid: state.LoginReducer.uid,
  });
};

const mapDispatchToProps = {
  changeLogin,
  changePassword,
  signIn,
};

export default connect(mapStateToProps, { ...mapDispatchToProps })(LoginScreen);
