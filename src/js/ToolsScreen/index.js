import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import Header from '../header';
import Footer from '../footer';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default class ToolsScreen extends Component {
  render() {
    return (
      <View style={ { flex: 1 } }>
        <Header title='Ferramentas' />
        <View style={styles.container}>
          <Text>New Features coming soon:</Text>
          <Text>1. Cronograma</Text>
          <Text>2. Wallpaper de usuários</Text>
          <Text>3. Checklist de Viagem</Text>
        </View>
        <Footer />
      </View>
    );
  }
}
