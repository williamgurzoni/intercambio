import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import firebase from 'firebase';
import { withNavigation } from 'react-navigation';

const style = StyleSheet.create({
  header: {
    height: 50,
    backgroundColor: '#56CCF2',
    paddingTop: 13,
    paddingLeft: 34,
    paddingRight: 34,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  title: {
    fontSize: 20,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    color: '#FEFEFE',
  },
});

class Header extends Component<props> {
  logOut() {
    firebase.auth().signOut()
      .then(() => {
        console.log('User logged out');
        this.props.navigation.navigate('Welcome');
      })
      .catch((err) => {
        console.error('Error to logout: ', err);
      });
  }

  render() {
    return (
      <View style={style.header}>
        <Text style={style.title}>{ this.props.title }</Text>
        <View>
          <TouchableWithoutFeedback
            onPress={ () => { this.logOut(); }}
          >
            <Text>Logout</Text>
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }
}

export default withNavigation(Header);
