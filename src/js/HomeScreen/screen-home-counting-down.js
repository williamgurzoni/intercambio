import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  countingDownDay: {
    marginTop: 35,
    color: '#2D9CDB',
    fontSize: 64,
    fontWeight: '400',
  },
  countingDownTime: {
    color: '#828282',
    fontSize: 36,
    fontWeight: '400',
  },
});

export default class ContingDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      days: 0,
      minutes: '',
    };

    // Define tripDate
    this.departureDate = (this.stringToDate(this.props.tripDate, 'DD/MM/YYYY', '/')).getTime();
  }

  componentDidUpdate() {
    this.departureDate = (this.stringToDate(this.props.tripDate, 'DD/MM/YYYY', '/')).getTime();
  }

  stringToDate(_date,_format,_delimiter) {
    var formatLowerCase=_format.toLowerCase();
    var formatItems=formatLowerCase.split(_delimiter);
    var dateItems=_date.split(_delimiter);
    var monthIndex=formatItems.indexOf("mm");
    var dayIndex=formatItems.indexOf("dd");
    var yearIndex=formatItems.indexOf("yyyy");
    var month=parseInt(dateItems[monthIndex]);
    month-=1;
    var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    return formatedDate;
  }


  componentDidMount() {
    setInterval(() => {
      this.calcCountingDownDays();
    }, 1000);
  }

  calcCountingDownDays() {
    const today = new Date();
    let diference = this.departureDate - today.getTime();
    diference = diference / 1000 / 60 / 60 / 24; // em dias com horas quebradas
    let hours = (diference - Math.floor(diference)) * 24;
    let minutes = (hours - Math.floor(hours)) * 60;
    let seconds = (minutes - Math.floor(minutes)) * 60;

    hours = (`00${Math.floor(hours)}`).slice(-2);
    minutes = (`00${Math.floor(minutes)}`).slice(-2);
    seconds = (`00${Math.floor(seconds)}`).slice(-2);

    const strMinutes = `${hours}:${minutes}:${seconds}`;
    this.setState({ days: Math.floor(diference), minutes: strMinutes });
  }

  render() {
    return (
      <View style={ styles.container }>
        <Text style={ styles.countingDownDay }>{ this.state.days } DIAS</Text>
        <Text style={ styles.countingDownTime }>{ this.state.minutes }</Text>
      </View>
    );
  }
}
