import React, { Component } from 'react';
import {
  View,
  StatusBar,
  ImageBackground,
  Text,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';

import { getUserProfile, getInspiringPhrase } from '../../actions/HomeActions';
import Footer from '../footer';
import Header from '../header';
import CountingDown from './screen-home-counting-down';
import styles from './style';

const homeBg = require('./../../assets/home-bg.jpg');

class HomeScreen extends Component<props> {
  constructor(props) {
    super(props);

    // Inicializa página Home
    this.props.getUserProfile(this.props.uid);
    this.props.getInspiringPhrase();
  }

  render() {
    return (
        <View style={ { flex: 1 } }>
          <StatusBar
            backgroundColor="#56CCF2"
            barStyle="light-content"
          />
          <Header title='Intercâmbio App' />
          <ImageBackground source={ homeBg } style={ styles.content }>
            <ScrollView>
              <View style={ styles.projectDescription }>
                <Text style={ styles.project }>Projeto: { this.props.exchangeType }</Text>
                <Text style={ styles.country }>{ this.props.country }</Text>
                <CountingDown tripDate={ this.props.tripDate } />
                <Text style={ styles.phrase }>
                  { `${this.props.inspiringPhrase.phrase} - autor: ${this.props.inspiringPhrase.author}` }
                </Text>
                <Text style={ styles.bold }>Não desista do seu sonho!</Text>
              </View>
              <View style={ styles.budgetView }>
                <Text style={ styles.budgetText }>Minhas economias</Text>
                <Text style={ styles.budgetValue }>R$ 7.500,00</Text>
                <Text style={ styles.budgetText }>Objetivo</Text>
                <Text style={ styles.budgetValue }>R$ 10.000,00</Text>
              </View>
            </ScrollView>
          </ImageBackground>
          <Footer />
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    uid: state.LoginReducer.uid,
    country: state.WizardReducer.country,
    tripDate: state.WizardReducer.tripDate,
    exchangeType: state.WizardReducer.exchangeType,
    inspiringPhrase: state.WizardReducer.inspiringPhrase,
  });
};

const mapDispatchToProps = {
  getUserProfile,
  getInspiringPhrase,
};

export default connect(mapStateToProps, { ...mapDispatchToProps })(HomeScreen);
