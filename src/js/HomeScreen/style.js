import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  content: {
    flex: 1,
    backgroundColor: '#FEFEFE',
  },
  projectDescription: {
    alignItems: 'center',
  },
  project: {
    marginTop: 5,
  },
  country: {
    fontSize: 32,
    marginTop: 5,
    color: '#4F4F4F',
  },
  phrase: {
    marginTop: 25,
    color: '#333333',
    fontSize: 16,
    width: 250,
    textAlign: 'center',
  },
  budgetView: {
    marginTop: 24,
    marginLeft: 34,
  },
  budgetText: {
    color: '#828282',
    fontSize: 16,
  },
  budgetValue: {
    color: '#4F4F4F',
    fontSize: 32,
    fontWeight: '500',
  },
  bold: {
    fontWeight: '500',
  },
});

export default styles;
