import React, { Component } from 'react';
import {
  View,
  ImageBackground,
  Text,
  ScrollView,
  TouchableOpacity,
  Picker,
} from 'react-native';
import { connect } from 'react-redux';
import DatePicker from 'react-native-datepicker';

import {
  setCountry,
  setTripDate,
  setExchangeType,
  setDetailsProfile,
} from '../../actions/WizardActions';

import styles from '../LoginScreen/style';
import { countries } from '../db';

const homeBg = require('../WelcomeScreen/assets/welcome-bg.jpg');

class LoginNewScreen extends Component<props> {
  constructor(props) {
    super(props);

    // Inicializa stato Redux para evitar cadastro em branco
    this.props.setCountry('Nova Zelândia');
    this.props.setTripDate('01/01/2020');
    this.props.setExchangeType('Estudo');
  }

  wizard() {
    const {
      country,
      tripDate,
      exchangeType,
      uid,
    } = this.props;
    this.props.setDetailsProfile(country, tripDate, exchangeType, uid);
  }

  render() {
    const pickerItem = countries.map((country) => {
      return <Picker.Item key={country.isoName} label={country.name} value={country.name} />;
    });

    return (
        <View style={ { flex: 1 } }>
          <ImageBackground source={ homeBg } style={ styles.ImageBackground }>
            <ScrollView>
              <View
                style={{
                  alignSelf: 'center',
                  flex: 1,
                  alignItems: 'stretch',
                  width: 280,
                }}>
                <Text
                  style={{
                    fontSize: 28,
                    fontWeight: 'bold',
                    color: '#FFFFFF',
                    alignSelf: 'center',
                  }}>
                  Intercâmbio Planner
                </Text>
                <Text style={{ textAlign: 'center', fontSize: 10, marginBottom: 50 }}>
                  @passagempradois
                </Text>
                <Text style={{ color: 'white' }}>
                  País de destino:
                </Text>
                <Picker
                  selectedValue={ this.props.country }
                  style={ styles.wizardTextInput }
                  onValueChange={ (itemValue, itemIndex) => this.props.setCountry(itemValue) } >
                  { pickerItem }
                </Picker>
                <Text style={{ color: 'white' }}>
                  Data prevista do Intercâmbio:
                </Text>
                <DatePicker
                  format="DD/MM/YYYY"
                  date={ this.props.tripDate }
                  onDateChange={ (tripDate) => { this.props.setTripDate(tripDate); } }
                  style={{
                    backgroundColor: '#F6F6F6',
                    height: 50,
                    marginBottom: 15,
                    borderColor: '#F6F6F6',
                    width: 280,
                    justifyContent: 'center',
                  }}
                  customStyles={{
                    dateInput: {
                      borderColor: '#F6F6F6',
                      fontSize: 17,
                      color: '#828282',
                    },
                  }}
                />
                <Text style={{ color: 'white' }}>
                  Tipo do Intercâmbio:
                </Text>
                <Picker
                  selectedValue={ this.props.exchangeType }
                  style={ styles.wizardTextInput }
                  onValueChange={ exchangeType => this.props.setExchangeType(exchangeType) } >
                  <Picker.Item label='Estudo' value='Estudo' />
                  <Picker.Item label='Ano Sabático' value='Ano Sabático' />
                  <Picker.Item label='Nomadismo Digital' value='Nomadismo Digital' />
                  <Picker.Item label='Cultural' value='Cultural' />
                  <Picker.Item label='Causas Sociais' value='Causas Sociais' />
                </Picker>
                <TouchableOpacity
                  style={ styles.btnStart }
                  onPress={ () => { this.wizard(); } }>
                  <View style={ styles.bgBtnStart }>
                    <Text style={ styles.txtBtnStart }>Próximo</Text>
                  </View>
                </TouchableOpacity>
                <Text
                  style={ this.props.newUserErrorMessage ? styles.errorMessage : null }
                >
                  { this.props.newUserErrorMessage }
                </Text>
              </View>
            </ScrollView>
          </ImageBackground>
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    uid: state.LoginReducer.uid,
    newUserErrorMessage: state.LoginReducer.newUserErrorMessage,
    country: state.WizardReducer.country,
    tripDate: state.WizardReducer.tripDate,
    exchangeType: state.WizardReducer.exchangeType,
  });
};

const mapDispatchToProps = {
  setCountry,
  setTripDate,
  setExchangeType,
  setDetailsProfile,
};

export default connect(mapStateToProps, { ...mapDispatchToProps })(LoginNewScreen);
