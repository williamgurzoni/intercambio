import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingBottom: 20,
  },
  topProfileView: {
    backgroundColor: '#FAFAFA',
    justifyContent: 'center',
    alignItems: 'center',
    height: 250,
  },
  photoWrapper: {
    backgroundColor: 'yellow',
    height: 160,
    width: 160,
    borderRadius: 90,
  },
  sectionTitle: {
    color: '#2D9CDB',
    fontSize: 17,
    marginBottom: 15,
  },
  centerProfileView: {
    backgroundColor: '#FAFAFA',
    marginTop: 10,
    padding: 10,
  },
  picker: {
    height: 50,
    backgroundColor: '#F2F2F2',
    marginBottom: 15,
    marginTop: 5,
  },
});

export default styles;
