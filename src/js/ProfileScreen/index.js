import React, { Component } from 'react';
import {
  View,
  Text,
  Picker,
  ScrollView,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import { connect } from 'react-redux';

import { setProfile, setDestination } from '../../actions/ProfileActions';
import Header from '../header';
import Footer from '../footer';
import styles from './style';

class ProfileScreen extends Component<props> {
  constructor(props) {
    super(props);

    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();
    const dateFormated = `${day}/${month}/${year}`;

    this.state = {
      dateFormated,
    };
  }

  setProfileCountry(itemValue) {
    this.props.setDestination(itemValue, this.props.uid, 'country');
  }

  setProfileExchangeType(itemValue) {
    this.props.setDestination(itemValue, this.props.uid, 'exchangeType');
  }

  setProfileTripDate(itemValue) {
    this.props.setDestination(itemValue, this.props.uid, 'tripDate');
  }

  setUserData(itemValue) {
    this.props.setProfile(itemValue, this.props.uid, 'birthDate');
  }

  render() {
    return (
      <View style={ { flex: 1 } }>
        <Header title='Profile' />
        <ScrollView style={styles.container}>
          <View style={ styles.topProfileView }>
            <View style={ styles.photoWrapper }>
            </View>
            <View style={ { marginTop: 15 } }>
              <Text style={ { fontSize: 22 } }>{ this.props.name }</Text>
            </View>
          </View>
          <View style={ styles.centerProfileView }>
            <Text style={ styles.sectionTitle }>Intercâmbio</Text>
            <View style={ { paddingHorizontal: 10 } } >
              <Text>País de destino</Text>
              <Picker
                selectedValue={ this.props.country }
                style={ styles.picker }
                onValueChange={ (itemValue, itemIndex) => this.setProfileCountry(itemValue) }>
                <Picker.Item label='Nova Zelândia' value='Nova Zelândia' />
                <Picker.Item label='Irlanda' value='Irlanda' />
                <Picker.Item label='Canadá' value='Canadá' />
              </Picker>
            </View>
            <View style={ { paddingHorizontal: 10 } } >
              <Text>Data estimada da viagem</Text>
              <DatePicker
                format="DD/MM/YYYY"
                date={ this.props.tripDate }
                onDateChange={ (tripDate) => { this.setProfileTripDate(tripDate); } }
                style={ [styles.picker, { justifyContent: 'center', width: 280 }] }
                customStyles={{
                  dateInput: {
                    borderColor: '#F2F2F2',
                    fontSize: 17,
                    color: '#828282',
                  },
                }}
              />
            </View>
            <View style={ { paddingHorizontal: 10 } } >
              <Text>Tipo do Intercâmbio</Text>
              <Picker
                selectedValue={ this.props.exchangeType }
                style={ styles.picker }
                onValueChange={
                  exchangeType => this.setProfileExchangeType(exchangeType)
                }>
                <Picker.Item label='Estudo' value='Estudo' />
                <Picker.Item label='Ano Sabático' value='Ano Sabático' />
                <Picker.Item label='Nomadismo Digital' value='Nomadismo Digital' />
                <Picker.Item label='Cultural' value='Cultural' />
                <Picker.Item label='Causas Sociais' value='Causas Sociais' />
              </Picker>
            </View>
            <Text style={ styles.sectionTitle }>Pessoal</Text>
            <View style={ { paddingHorizontal: 10 } } >
              <Text>Data de Nascimento</Text>
              <DatePicker
                format='DD/MM/YYYY'
                date={ this.props.birthDate }
                placeholder='Selecione a Data'
                maxDate={ this.state.dateFormated }
                onDateChange={ (birthDate) => { this.setUserData(birthDate); } }
                style={ [styles.picker, { justifyContent: 'center', width: 280 }] }
                customStyles={{
                  dateInput: {
                    borderColor: '#F2F2F2',
                    fontSize: 17,
                    color: '#828282',
                  },
                }}
              />
            </View>
          </View>
        </ScrollView>
        <Footer />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    uid: state.LoginReducer.uid,
    country: state.WizardReducer.country,
    tripDate: state.WizardReducer.tripDate,
    exchangeType: state.WizardReducer.exchangeType,
    birthDate: state.WizardReducer.birthDate,
    name: state.WizardReducer.name,
  });
};

const mapDispatchToProps = {
  setProfile,
  setDestination,
};

export default connect(mapStateToProps, { ...mapDispatchToProps })(ProfileScreen);
