import { createStackNavigator, createAppContainer } from 'react-navigation';

import WelcomeScreen from './WelcomeScreen';
import LoginScreen from './LoginScreen';
import LoginNewScreen from './LoginNewScreen';
import LoginWizard from './LoginWizard';
import HomeScreen from './HomeScreen';
import ProfileScreen from './ProfileScreen';
import ToolsScreen from './ToolsScreen';
import InfoScreen from './InfoScreen';

const AppNavigator = createStackNavigator(
  {
    Welcome: WelcomeScreen,
    Login: LoginScreen,
    LoginNew: LoginNewScreen,
    Wizard: LoginWizard,
    Home: HomeScreen,
    Profile: ProfileScreen,
    Tools: ToolsScreen,
    Info: InfoScreen,
  },
  {
    initialRouteName: 'Welcome',
    headerMode: 'none',
  },
);

const AppRoutes = createAppContainer(AppNavigator);

export default AppRoutes;
