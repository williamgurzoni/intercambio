/*
Função responsável por permitir a navegação em components sem props do navigator.
Define um top level na aplicação.

Para usar:

// any js module
import NavigationService from 'path-to-NavigationService.js';

// ...
NavigationService.navigate('ChatScreen', { userName: 'Lucy' });

*/

import { NavigationActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
}

export default {
  navigate,
  setTopLevelNavigator,
};
