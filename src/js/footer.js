import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import { withNavigation } from 'react-navigation';

const style = StyleSheet.create({
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#2D9CDB',
    height: 50,
    paddingHorizontal: 35,
    alignItems: 'center',
  },
  menuIcon: {
    alignItems: 'center',
  },
  menuText: {
    color: '#FEFEFE',
  },
  icon: {
    height: 30,
    width: 30,
  },
});

const iconTools = require('./../assets/icon-tools.png');
const iconMoney = require('./../assets/icon-money.png');
const iconHome = require('./../assets/icon-pin.png');
const iconInfo = require('./../assets/icon-info.png');
const iconProfile = require('./../assets/icon-profile.png');

class Footer extends Component<props> {
  render() {
    return (
      <View style={ style.footer }>
        <TouchableOpacity
          style={ style.menuIcon }
          onPress={ () => { this.props.navigation.navigate('Tools'); } }
        >
          <Image style={ style.icon } source={ iconTools } />
        </TouchableOpacity>
        <View style={ style.menuIcon }>
          <Image style={ style.icon } source={ iconMoney } />
        </View>
        <TouchableOpacity
          style={ style.menuIcon }
          onPress={ () => { this.props.navigation.navigate('Home'); } }
        >
          <Image style={{ height: 35, width: 35 }} source={ iconHome } />
        </TouchableOpacity>
        <TouchableOpacity
          style={ style.menuIcon }
          onPress={ () => { this.props.navigation.navigate('Info'); } }
        >
          <Image style={ style.icon } source={ iconInfo } />
        </TouchableOpacity>
        <TouchableOpacity
          style={ style.menuIcon }
          onPress={ () => { this.props.navigation.navigate('Profile'); } }
        >
          <Image style={ style.icon } source={ iconProfile } />
        </TouchableOpacity>
      </View>
    );
  }
}

export default withNavigation(Footer);
