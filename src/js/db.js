// Todo: Change this to AsyncStorage!

export const countries = [
  {
    'name': 'Nova Zelândia',
    'isoName': 'NZ',
    'timeZone': 'Pacific/Auckland',
    'principalCity': 'Auckland',
    'principalCityId': '2193734',
    'capital': 'Wellington',
    'currency': 'NZD',
  },
  {
    'name': 'Canadá',
    'isoName': 'CA',
    'timeZone': 'America/Toronto',
    'principalCity': 'Toronto',
    'principalCityId': '6167865',
    'capital': 'Ottawa',
    'currency': 'CAD',
  },
  {
    'name': 'Irlanda',
    'isoName': 'IE',
    'timeZone': 'Europe/Dublin',
    'principalCity': 'Dublin',
    'principalCityId': '2964574',
    'capital': 'Dublin',
    'currency': 'EUR',
  },
  {
    'name': 'Austrália',
    'isoName': 'AU',
    'timeZone': 'Australia/Sydney',
    'principalCity': 'City of Sydney',
    'principalCityId': '6619279',
    'capital': 'Camberra',
    'currency': 'AUD',
  },
]
