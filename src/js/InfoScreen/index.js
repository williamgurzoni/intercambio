import React, { Component } from 'react';
import {
  View,
  Text,
  ScrollView,
  TextInput,
  Image,
  ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';

import styles from './style';
import { currencyApi, weatherApi } from '../api';
import Header from '../header';
import Footer from '../footer';
import Clock from './clock';
import { countries } from '../db';

class InfoScreen extends Component<props> {
  constructor(props) {
    super(props);

    this.state = {
      rate: 0,
      localCurrencyRate: 1000,
      exchangeCurrencyRate: 0,
      fromCurrency: 'BRL',
      toCurrency: '',
      temp: 0,
      cityId: '',
      cityName: '',
      iconTemp: '',
      isoCountryName: 'NZ',
      isWeatherReady: false,
      isCurrencyReady: false,
      isTimeReady: false,
    };
  }

  getWeather() {
    weatherApi(this.state.cityId)
      .then((res) => {
        this.setState({ temp: res.data.data.main.temp });
        this.setState({ cityName: res.data.data.name });
        this.setState({ iconTemp: res.data.data.weather[0].icon });

        this.setState({ isWeatherReady: true });
      })
      .catch((e) => {
        console.error('Erro temp', e);
      });
  }

  getExchangeCurrency() {
    currencyApi(this.state.fromCurrency, this.state.toCurrency)
      .then((res) => {
        const rate = res.data.data['Realtime Currency Exchange Rate']['5. Exchange Rate'];
        this.calcRate(rate);
      })
      .catch((e) => {
        console.error(e);
        return false;
      });
  }

  calcRate(rateStr) {
    // Obs. Verificado que a diferenca do dolar turismo (NZ) e o Comercial é de 12%..
    // Verificar melhor forma de fazer esta tratativa para o valor ser mais aproximado.

    let rate = Number(rateStr) * 0.89;
    rate = rate.toFixed(4);
    this.setState({ rate });
    this.updateExchangeCurrencyRate();
  }

  updateExchangeCurrencyRate(localCurrencyRate) {
    if (!localCurrencyRate) {
      localCurrencyRate = this.state.localCurrencyRate;
    }
    const exchangeCurrencyRate = localCurrencyRate * this.state.rate;
    this.setState({ exchangeCurrencyRate });

    this.setState({ isCurrencyReady: true });
  }

  onChangeTextLocalCurrency(text) {
    const localCurrencyRate = Number(text);
    this.setState({ localCurrencyRate });
    this.updateExchangeCurrencyRate(localCurrencyRate);
  }

  updateStatesAsync() {
    return new Promise((resolve) => {
      const countryInfo = countries.filter((c) => {
        return c.name === this.props.country;
      });
      this.setState({
        toCurrency: countryInfo[0].currency,
        cityId: countryInfo[0].principalCityId,
        cityName: countryInfo[0].principalCity,
        isoCountryName: countryInfo[0].isoName,
        isTimeReady: true,
      });
      resolve();
    });
  }

  async updateStates() {
    await this.updateStatesAsync();
    this.getExchangeCurrency();
    this.getWeather();
  }

  componentDidMount() {
    // Update the variables to screen
    this.updateStates();
  }

  render() {
    const { isWeatherReady, isCurrencyReady, isTimeReady } = this.state;
    if (isWeatherReady && isCurrencyReady && isTimeReady) {
      return (
        <View style={{ flex: 1 }}>
          <Header title='Informações Úteis' />
          <ScrollView style={ styles.container }>
          <View style={ styles.section }>
            <Text style={{ fontSize: 22, textAlign: 'center', fontWeight: '500' }}>
              { this.props.country } + { this.state.cityName }
            </Text>
          </View>
            <View style={ styles.section }>
              <Text style={ styles.sectionTitle }>Hora Local</Text>
              <Clock country={ this.state.isoCountryName } />
            </View>
            <View style={ styles.section }>
              <Text style={ styles.sectionTitle }>Clima</Text>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center' }}>
                  <Image
                    style={{ width: 120, height: 120 }}
                    source={{ uri: `http://openweathermap.org/img/w/${this.state.iconTemp}.png` }}
                  />
                </View>
                <View style={{ flex: 2 }}>
                  <Text style={{ fontSize: 36 }}>{ (this.state.temp).toFixed(0) } ºC</Text>
                  <Text style={{ fontSize: 36 }}>{ (this.state.temp * 1.8 + 32).toFixed(0) } ºF</Text>
                  <Text>Primavera</Text>
                </View>
              </View>
            </View>
            <View style={ styles.section }>
              <Text style={ styles.sectionTitle }>Conversão de Moeda</Text>
              <View style={{ flexDirection: 'row', paddingHorizontal: 60, justifyContent: 'space-between' }}>
                <Text>{ this.state.fromCurrency }</Text>
                <Text>{ this.state.toCurrency }</Text>
              </View>
              <View style={{ flexDirection: 'row' }}>
                <View style={{ backgroundColor: '#F6F6F6', flex: 2, marginVertical: 10 }}>
                  <TextInput
                    keyboardType='numeric'
                    style={{ fontSize: 18, textAlign: 'center' }}
                    value={ String(this.state.localCurrencyRate) }
                    onChangeText={ (text) => { this.onChangeTextLocalCurrency(text); }}
                  />
                </View>
                <View style={{ flex: 0.5, alignItems: 'center', marginTop: 10 }}>
                  <Text style={{ fontSize: 36 }}>=</Text>
                </View>
                <View style={{ backgroundColor: '#F6F6F6', flex: 2, marginVertical: 10 }}>
                  <TextInput
                    keyboardType='numeric'
                    style={{ fontSize: 18, textAlign: 'center' }}
                    value={ String((this.state.exchangeCurrencyRate).toFixed(2)) }
                    onChangeText={ () => { this.updateExchangeCurrencyRate(); }}
                  />
                </View>
              </View>
            </View>
            <View style={ styles.section }>
              <Text style={ styles.sectionTitle }>Site da Imigração</Text>
              <Text style={{ textAlign: 'center' }}>www.imigration.govt.nz</Text>
            </View>
            <Text>New Features coming soon:</Text>
            <Text>1. Hora e clima local</Text>
            <Text>2. Cotação / Conversão de moeda</Text>
            <Text>3. Informações sobre o local</Text>
            <Text>4. Documentação necessária</Text>
            <Text>5. Site da imigração</Text>
            <Text>6. Vacinas</Text>
          </ScrollView>
          <Footer />
        </View>
      );
    }
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="#2D9CDB" />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return ({
    country: state.WizardReducer.country,
  });
};

const mapDispatchToProps = {
};

export default connect(mapStateToProps, { ...mapDispatchToProps })(InfoScreen);
