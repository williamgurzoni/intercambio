import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  section: {
    marginBottom: 10,
    paddingBottom: 20,
    borderBottomColor: '#F2F2F2',
    borderBottomWidth: 1,
    borderStyle: 'solid',
  },
  sectionTitle: {
    color: '#2D9CDB',
    fontSize: 17,
    marginBottom: 15,
  },
});

export default styles;
