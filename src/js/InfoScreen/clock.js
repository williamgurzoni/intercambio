import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';

import moment from 'moment-timezone';

import { countries } from '../db';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  localTime: {
    fontSize: 36,
    textAlign: 'center',
  },
  timezone: {
    fontSize: 13,
    textAlign: 'center',
  },
});

export default class Clock extends Component<props> {
  constructor(props) {
    super(props);

    this.state = {
      time: '',
      gmtInfo: '',
      timeZone: '',
      isoName: '',
    };
  }

  setTime() {
    const { timeZone } = countries.filter(country => country.isoName === this.props.country)[0];
    const gmtInfo = moment().tz(timeZone).format('[GMT]Z');
    this.setState({ gmtInfo });
    this.setState({ timeZone });
  }

  updateTime() {
    const time = moment().tz(this.state.timeZone).format('hh:mm A');
    this.setState({ time });
  }

  componentDidMount() {
    this.setTime();
    setInterval(() => {
      this.updateTime();
    }, 1000);
  }

  render() {
    console.log('rederizando relogio');
    return (
      <View style={ styles.container }>
        <Text style={ styles.localTime }>{ this.state.time }</Text>
        <Text style={ styles.timezone }>{ `${this.state.timeZone} - ${this.state.gmtInfo}` }</Text>
      </View>
    );
  }
}
