const INITIAL_STATE = {
  country: 'Irlanda',
  tripDate: '',
  exchangeType: '',
  birthDate: '',
  name: '',
  inspiringPhrase: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'country':
      return { ...state, country: action.payload };
    case 'tripDate':
      return { ...state, tripDate: action.payload };
    case 'exchangeType':
      return { ...state, exchangeType: action.payload };
    case 'birthDate':
      return { ...state, birthDate: action.payload };
    case 'getUserProfileSuccess':
      return {
        ...state,
        country: action.payload.destination.country,
        tripDate: action.payload.destination.tripDate,
        exchangeType: action.payload.destination.exchangeType,
        name: action.payload.profile.name,
      };
    case 'inspiringPhrase':
      return { ...state, inspiringPhrase: action.payload };
    default:
      return state;
  }
};
