import { combineReducers } from 'redux';

import LoginReducer from './LoginReducer';
import WizardReducer from './WizardReducer';

export default combineReducers({
  LoginReducer,
  WizardReducer,
});
