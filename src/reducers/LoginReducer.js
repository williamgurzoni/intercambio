const INITIAL_STATE = {
  name: 'William',
  login: 'william.luizat@gmail.com',
  password: 'luizat',
  newUserErrorMessage: '',
  loginErrorMessage: '',
  uid: '',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'changeName':
      return { ...state, name: action.payload };
    case 'changeLogin':
      return { ...state, login: action.payload };
    case 'changePassword':
      return { ...state, password: action.payload };
    case 'newUser':
      return { ...state, uid: action.payload };
    case 'newUserErrorMessage':
      return { ...state, newUserErrorMessage: action.payload };
    case 'loginErrorMessage':
      return { ...state, loginErrorMessage: action.payload };
    case 'loginSuccess':
      return { ...state, uid: action.payload };
    default:
      return state;
  }
};
