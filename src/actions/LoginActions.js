import firebase from 'firebase';
import NavigationService from '../js/navigation-service';

export const changeLogin = login => ({
  type: 'changeLogin',
  payload: login,
});

export const changeName = name => ({
  type: 'changeName',
  payload: name,
});

export const changePassword = password => ({
  type: 'changePassword',
  payload: password,
});

const createNewUserSuccess = (user, dispatch) => {
  dispatch({ type: 'newUser', payload: user.user.uid });
  NavigationService.navigate('Wizard');
};

const createNewUserFail = (err, dispatch) => {
  dispatch({ type: 'newUserErrorMessage', payload: err.message });
};

export const createNewUser = (name, login, password) => {
  return (dispatch) => {
    firebase.auth().createUserWithEmailAndPassword(login, password)
      .then((user) => {
        firebase.database().ref(`user/${user.user.uid}/profile/`).set({ name })
          .then(createNewUserSuccess(user, dispatch));
      })
      .catch((err) => { createNewUserFail(err, dispatch); });
  };
};

const signInFail = (err, dispatch) => {
  console.error(`Falha no login: ${err.message}`)
  dispatch({ type: 'loginErrorMessage', payload: err.message });
};

const signInSuccess = (user, dispatch) => {
  dispatch({ type: 'loginSuccess', payload: user.user.uid });
  NavigationService.navigate('Home');
};

export const signIn = (login, password) => {
  return (dispatch) => {
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(() => {
        firebase.auth().signInWithEmailAndPassword(login, password)
          .then((user) => { signInSuccess(user, dispatch); })
          .catch((err) => {
            console.error(`Deu ruim no login: ${err.message}`);
            signInFail(err, dispatch);
          });
      })
      .catch((err) => {
        console.error('Error on persisting auth', err);
      });
  };
};
