import firebase from 'firebase';

const getUserProfileSuccess = (snapshot, dispatch) => {
  console.log(snapshot.val());
  dispatch({ type: 'getUserProfileSuccess', payload: snapshot.val() });
};

const getUserProfileFail = (err, dispatch) => {
  console.log(err);
  dispatch({ type: 'getUserProfileFail' });
};

export const getUserProfile = (uid) => {
  console.log('Cheguei até aqui', uid);
  return (dispatch) => {
    firebase.database().ref(`user/${uid}`).on('value', (snapshot) => {
      getUserProfileSuccess(snapshot, dispatch);
    }, (err) => {
      getUserProfileFail(err, dispatch);
    });
  };
};

const getInspiringPhraseSuccess = (snapshot, dispatch) => {
  if (snapshot.val()) {
    const phrases = snapshot.val();
    const phrase = phrases[Math.floor(Math.random() * phrases.length)];
    dispatch({ type: 'inspiringPhrase', payload: phrase });
  }
};

export const getInspiringPhrase = () => {
  return (dispatch) => {
    firebase.database().ref('inspiringphrases').once('value', (snapshot) => {
      getInspiringPhraseSuccess(snapshot, dispatch);
    });
  };
};
