import firebase from 'firebase';

const setInfoFail = (err, dispatch) => {
  console.log('Erro ao gravar detalhes do profile: ');
  console.log(err.message);
  dispatch({ type: 'setInfoFail' });
};

const setInfoSuccess = (payload, dispatch, type) => {
  dispatch({ type, payload });
};

export const setDestination = (value, uid, type) => {
  return (dispatch) => {
    firebase.database().ref(`user/${uid}/destination/${type}`).set(value)
      .then(setInfoSuccess(value, dispatch, type))
      .catch(err => setInfoFail(err, dispatch));
  };
};

const setProfileSucess = (payload, dispatch, type) => {
  dispatch({ type, payload });
};

const setProfileFail = (err, dispatch) => {
  dispatch({ type: 'setProfileFail' });
};

export const setProfile = (value, uid, type) => {
  return (dispatch) => {
    firebase.database().ref(`user/${uid}/profile/${type}`).set(value)
      .then(setProfileSucess(value, dispatch, type))
      .catch(err => setProfileFail(err, dispatch));
  };
};
