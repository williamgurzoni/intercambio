import firebase from 'firebase';
import NavigationService from '../js/navigation-service';

export const setCountry = country => ({
  type: 'country',
  payload: country,
});

export const setTripDate = tripDate => ({
  type: 'tripDate',
  payload: tripDate,
});

export const setExchangeType = exchangeType => ({
  type: 'exchangeType',
  payload: exchangeType,
});

const setDetailsProfileFail = (err, dispatch) => {
  console.log('Erro ao gravar detalhes do profile: ');
  console.log(err.message);
  dispatch({ type: 'setDetailsProfileFail' });
};

const setDetailsProfileSuccess = (value, dispatch) => {
  console.log(value);
  dispatch({ type: 'setDetailsProfileSuccess' });
  NavigationService.navigate('Home');
};

export const setDetailsProfile = (country, tripDate, exchangeType, uid) => {
  const destination = { country, tripDate, exchangeType };
  return (dispatch) => {
    firebase.database().ref(`user/${uid}/destination`).set({ ...destination })
      .then(setDetailsProfileSuccess('Gravado exchangeType com sucesso', dispatch))
      .catch(err => setDetailsProfileFail(err, dispatch));
  };
};
